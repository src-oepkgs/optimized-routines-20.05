Name:	optimized-routines
Version:	20.05
Release:	1
Summary:	Optimized implementations of various library functions for ARM architecture processors.
License:	MIT
URL:	https://github.com/ARM-software/optimized-routines
Source0:	%{name}-%{version}.tar.gz
ExclusiveArch:	aarch64
BuildRequires:	make,gcc,mpfr-devel,libmpc-devel,gmp-devel,glibc-static

%description
Optimized implementations of various library functions for ARM architecture processors.

#skip debuginfo packages
%global debug_package %{nil}

%prep
%autosetup -p1
cp config.mk.dist config.mk
sed -i "s/SUBS = math string networking/SUBS = math string/g" config.mk

%build
%make_build

%install
%make_install libdir=%{_libdir}

%check
make check

%files
%license LICENSE
%doc README
%{_includedir}/*
%{_libdir}/*

%changelog
* Sat Sep 3 2022 xiongzhou <xiongzhou4@huawei.com> - 20.05-1
- Type:Init
- ID:NA
- SUG:NA
- DESC:Init optimized-routines repository
* Thu Sep 14 2023 shenjinyu <shenjinyu1@h-partners.com> - 20.05-2
- ID:NA
- SUG:NA
- DESC:Add 22.03 SP1 version
